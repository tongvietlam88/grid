$( document ).ready(function() {
	$(".arrow_video").click(function() {
		$("body").addClass("visible");
		$(this).parents(".content_video").find(".popup_content").addClass("open");
		$(this).parents(".content_video").find("video")[0].play();
	});
	$(".close_video").click(function() {
		$("body").removeClass("visible");
		$(this).parents(".content_video").find(".popup_content").removeClass("open");
		$(this).parents(".content_video").find("video")[0].pause();
	});
});