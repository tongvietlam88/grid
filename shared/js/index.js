$(".has-sub").hover(
	function() {
		$( this ).addClass("hover");
		$('header').addClass("hover-sub");
	}, function() {
		$( this ).removeClass("hover");
		$('header').removeClass("hover-sub");
	}
);

$('.key-slide').slick({
	autoplay: true,
	dots: true,
	arrows: false,
	infinite: true,
	fade: true,
	autoplaySpeed: 3000,
	speed: 2000,
	slidesToShow: 1,
	swipe: false,
	pauseOnFocus: false,
	pauseOnHover: false,
	pauseOnDotsHover: false,
});

$('.news-slide').slick({
	infinite: true,
	speed: 300,
	slidesToShow: 4,
	slidesToScroll: 4,
	centerPadding: '37px',
	responsive: [
    {
		breakpoint: 991,
		settings: {
		    slidesToShow: 3,
		    slidesToScroll: 3,
      	}
    },
    {
		arrows: true,
		breakpoint: 750,
		settings: {
		    slidesToShow: 2,
		    slidesToScroll: 2
		}
    },
    {
		arrows: true,
		breakpoint: 370,
		settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
		}
    }
    ]
});