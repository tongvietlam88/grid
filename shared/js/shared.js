$( document ).ready(function() {
	$(".arrow_video").click(function() {
		$("body").addClass("visible");
		$(this).parents(".content_video").find(".popup_content").addClass("open");
		$(this).parents(".content_video").find("video")[0].play();
	});
	$(".close_video").click(function() {
		$("body").removeClass("visible");
		$(this).parents(".content_video").find(".popup_content").removeClass("open");
		$(this).parents(".content_video").find("video")[0].pause();
	});

	$('.product-slide').slick({
		autoplay: true,
		dots: true,
		arrows: true,
		infinite: true,
		fade: false,
		autoplaySpeed: 2000,
		speed: 1000,
		slidesToShow: 1,
		swipe: false,
		pauseOnFocus: false,
		pauseOnHover: false,
		pauseOnDotsHover: false,
	});
});


